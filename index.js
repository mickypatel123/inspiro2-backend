const config = require('config');
const Joi = require('joi');
const express = require('express');
const mongoose = require('mongoose');
const Fawn = require('fawn');

const registration = require('./routes/registration');
const campaigner = require('./routes/campaigner');
const login = require('./routes/login');
const paymentsAnalysis = require('./routes/payments');
const college = require('./routes/college');
const resetcounter = require('./routes/resetcounter');
const collegeRegister = require('./routes/collegelist');
const analytics = require('./routes/analytics');
const modify = require('./routes/modify');
const app = express();
Joi.objectId = require('joi-objectid')(Joi);

app.disable('x-powered-by');

if (!config.has('jwtPrivateKey')) {
	console.error('JWT Private Key is not defined...Closing Application');
	process.exit(1);
}

mongoose
	.connect(config.get('dbUrl'), {
		useNewUrlParser: true,
		useCreateIndex: true,
		useFindAndModify: false
	})
	.then(() => console.log('Connected to Database'))
	.catch((err) => console.error('Error connecting database', err));
Fawn.init(mongoose);

app.use(express.json());
app.use('/api/registration', registration);
app.use('/api/campaigner', campaigner);
app.use('/api/login', login);
app.use('/api/paymentsAnalysis', paymentsAnalysis);
app.use('/api/college', college);
app.use('/api/resetcounter', resetcounter);
app.use('/api/collegeregister', collegeRegister);
app.use('/api/analytics', analytics);
app.use('/api/modify', modify);

const port = process.env.PORT || 3000;
app.listen(port, () => console.log('Listening on port ', port));
