const nodemailer = require('nodemailer');
const handlebars = require('handlebars');
const fs = require('fs');

let readHTMLFile = function(path, callback) {
	fs.readFile(path, { encoding: 'utf-8' }, function(err, html) {
		if (err) {
			throw err;
			callback(err);
		} else {
			callback(null, html);
		}
	});
};

let transporter = nodemailer.createTransport({
	service: 'gmail',
	auth: {
		user: 'ieee@adit.ac.in',
		pass: 'Ieee@d!7Sb2019'
	}
});
module.exports = {
	transporter,
	sendMail: async (participant, cashAmount) => {
		readHTMLFile('./src/mail-tempate.html', function(err, html) {
			let template = handlebars.compile(html);
			let replacements = {
				firstname: participant.firstname,
				lastname: participant.lastname,
				mobile: participant.mobile,
				email: participant.email,
				college: participant.college,
				cashamount: cashAmount
			};

			let htmlToSend = template(replacements);
			let mailOptions = {
				from: 'ieee@adit.ac.in',
				to: participant.email,
				subject: 'INSPIRO 2.0 : Registration Successful',
				html: htmlToSend
			};
			transporter.sendMail(mailOptions, function(error, response) {
				if (error) {
					console.log(error);
				} else {
					console.log('Email Sent : ', response.envelope.to, response.response);
				}
			});
		});
	}
};

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
