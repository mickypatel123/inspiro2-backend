const Joi = require('joi');
const bcrypt = require('bcrypt');

// const _ = require('lodash');
const express = require('express');

// const mongoose = require('mongoose');
const { Campaigner } = require('../models/campaigner');

const router = express.Router();
router.post('/', async (req, res) => {
	const { error } = validate(req.body);
	if (error) {
		const errorString = error.details[0].message.replace(/\"/g, '');
		return res.send({ auth: errorString, status: true });
	}

	let campaigner = await Campaigner.findOne({ mobile: req.body.mobile });
	if (!campaigner) return res.send({ auth: 'Invalid Credetials', status: true });

	const validPassword = await bcrypt.compare(req.body.password, campaigner.password);
	if (!validPassword) return res.send({ auth: 'Invalid Credetials', status: true });

	try {
		await Campaigner.updateOne(
			{ mobile: req.body.mobile },
			{
				$set: {
					non_ieee_registrationsToday: 0,
					ieee_registrationsToday: 0,
					amountCollectedToday: 0
				}
			}
		);
		res.send({ auth: 'Counter has been reset', status: true });
	} catch (ex) {
		console.log(ex);
		res.send({ auth: 'Internal Server Error', status: false });
	}
});
function validate(req) {
	const schema = {
		mobile: Joi.number()
			.integer()
			.required(),
		password: Joi.string().required()
	};
	return Joi.validate(req, schema);
}
module.exports = router;
