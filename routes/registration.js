const moment = require('moment-timezone');
const _ = require('lodash');
const express = require('express');
const jwt = require('jsonwebtoken');
const config = require('config');
const sendmail = require('../service/sendmail');
const mongoose = require('mongoose');
const Fawn = require('fawn');
const { Participant, validate } = require('../models/participant');
const { Payment } = require('../models/payment');
const auth = require('../middleware/auth');
let memType;
let cashAmount;
const router = express.Router();

router.post('/', auth, async (req, res) => {
	const { error } = validate(req.body);
	if (error) {
		const errorString = error.details[0].message.replace(/\"/g, '');
		return res.status(200).send({ auth: errorString, status: true });
	}
	let participant = await Participant.findOne({
		$or: [{ mobile: req.body.mobile }, { email: req.body.email }]
	});
	if (participant)
		return res.status(200).send({ auth: 'Participant already registered', status: true });
	participant = new Participant(
		_.pick(req.body, [
			'firstname',
			'lastname',
			'email',
			'mobile',
			'college',
			'registrationType',
			'paymentType',
			'membership'
		])
	);
	let registrationType = req.body.registrationType;
	participant.membership !== undefined ? (memType = 'IEEE') : (memType = 'non-IEEE');
	participant.membershipType = memType;
	switch (registrationType) {
		case 'Young Professional':
			cashAmount = config.get(memType + '_fee_yp');
			break;
		default:
			cashAmount = config.get(memType + '_fee_student');
			break;
	}
	const token = req.header('x-auth-token');
	const participantRegisteredBy = jwt.verify(token, config.get('jwtPrivateKey'))._id;
	participant.registeredBy = participantRegisteredBy;

	try {
		new Fawn.Task().save('participants', participant).run({ useMongoose: true });
		if (participant.membership !== undefined) {
			new Fawn.Task()
				.update(
					'campaigners',
					{ _id: participantRegisteredBy },
					{
						$inc: {
							ieee_registrationsToday: 1,
							amountCollectedToday: cashAmount
						}
					}
				)
				.run({ useMongoose: true });
		} else {
			new Fawn.Task()
				.update(
					'campaigners',
					{ _id: participantRegisteredBy },
					{
						$inc: {
							non_ieee_registrationsToday: 1,
							amountCollectedToday: cashAmount
						}
					}
				)
				.run({ useMongoose: true });
		}

		let payment = new Payment({
			date: moment()
				.tz('Asia/Kolkata')
				.format('MMM Do YYYY'),
			time: moment()
				.tz('Asia/Kolkata')
				.format('h:mm a'),
			participantId: participant._id,
			campaignerId: participantRegisteredBy,
			membershipType: memType,
			registrationType: participant.registrationType,
			paymentType: participant.paymentType,
			amount: cashAmount
		});
		await new Fawn.Task().save('payments', payment).run({ useMongoose: true });

		console.log(
			'\x1b[36m%s\x1b[32m%s\x1b[0m',
			'New Registration : ',
			participant.firstname + ' ' + participant.lastname
		);
		await sendmail.sendMail(participant, cashAmount);

		res.send({ auth: 'Registraion Successful', status: true });
	} catch (ex) {
		console.log(ex);
		res.send({ auth: 'Internal Server Error', status: false });
	}
});

module.exports = router;
