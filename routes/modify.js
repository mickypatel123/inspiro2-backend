const moment = require('moment-timezone');
const _ = require('lodash');
const express = require('express');
const config = require('config');
const sendmail = require('../service/sendmail');
const mongoose = require('mongoose');
const Fawn = require('fawn');
const { Participant, findvalidation } = require('../models/participant');
const { Payment } = require('../models/payment');
const { Campaigner } = require('../models/campaigner');
const auth = require('../middleware/auth');
const router = express.Router();

router.post('/find', async (req, res) => {
	let { error } = findvalidation(req.body);
	if (error) {
		const errorString = error.details[0].message.replace(/\"/g, '');
		return res.status(200).send({ auth: errorString, status: true });
	}
	let participant = await Participant.findOne({
		$or: [
			{ firstname: req.body.firstname },
			{ lastname: req.body.lastname },
			{ mobile: req.body.mobile },
			{ email: req.body.email }
		]
	});
	let paymentstatus = await Payment.findOne({ participantId: participant._id });

	res.send({ participant: participant, payment: paymentstatus });
});

router.post('/basic', async (req, res) => {
	if (req.body.firstname === '') delete req.body.firstname;
	if (req.body.lastname === '') delete req.body.lastname;
	if (req.body.mobile === '') delete req.body.mobile;
	if (req.body.college === '') delete req.body.college;

	await Participant.findOneAndUpdate({ _id: req.body.id }, { $set: req.body }, { new: true });
	let doc = await Participant.findOne({ _id: req.body.id });
	res.send(doc);
});

router.post('/email', async (req, res) => {
	await Participant.findOneAndUpdate(
		{ _id: req.body.id },
		{ $set: { email: req.body.email } },
		{ new: true }
	);
	let doc = await Participant.findOne({ _id: req.body.id });
	let cashAmount = await Payment.findOne({ participantId: req.body.id });

	sendmail.sendMail(doc, cashAmount.amount);
	res.send(doc);
});

router.post('/payment', async (req, res) => {
	await Participant.findOneAndUpdate(
		{ _id: req.body.id },
		{ $set: { paymentType: req.body.paymentType } },
		{ new: true }
	);
	await Payment.findOneAndUpdate(
		{ participantId: req.body.id },
		{ $set: { paymentType: req.body.paymentType } },
		{ new: true }
	);
});

router.post('/membership', async (req, res) => {
	let oldPayment = await Payment.findOne({ participantId: req.body.id });
	let oldAmount = oldPayment.amount;
	let newAmount;
	let participant = await Participant.findOneAndUpdate(
		{ _id: req.body.id },
		{ $set: { registrationType: req.body.registrationType } },
		{ new: true }
	);
	let payment = await Payment.findOneAndUpdate(
		{ participantId: req.body.id },
		{ $set: { registrationType: req.body.registrationType } },
		{ new: true }
	);
	payment.registrationType == 'Student'
		? (newAmount = config.get(payment.membershipType + '_fee_student'))
		: (newAmount = config.get(payment.membershipType + '_fee_yp'));

	let task1 = await Payment.findOneAndUpdate(
		{ participantId: req.body.id },
		{ $inc: { amount: -oldAmount } },
		{ new: true }
	);
	let task2 = await Campaigner.findOneAndUpdate(
		{ _id: payment.campaignerId },
		{ $inc: { amountCollectedToday: -oldAmount } },
		{ new: true }
	);

	let task3 = await Payment.findOneAndUpdate(
		{ participantId: req.body.id },
		{ $inc: { amount: newAmount } },
		{ new: true }
	);
	let task4 = await Campaigner.findOneAndUpdate(
		{ _id: payment.campaignerId },
		{ $inc: { amountCollectedToday: newAmount } },
		{ new: true }
	);
	sendmail.sendMail(participant, task3.amount);
	res.send({ participant: participant, payment: task3, campaigner: task4 });
});

module.exports = router;
