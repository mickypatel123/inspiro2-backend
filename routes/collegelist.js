const _ = require('lodash');
const express = require('express');

const mongoose = require('mongoose');
const { College } = require('../models/college');

const router = express.Router();
router.post('/', async (req, res) => {
	let college = await College.findOne({ collegeName: req.body.collegeName });
	if (college) return res.send({ auth: 'College already registered', status: true });
	college = new College(_.pick(req.body, ['collegeName']));

	await college.save();
	res.send({ auth: 'Successfully Registered', status: true });
});

module.exports = router;
