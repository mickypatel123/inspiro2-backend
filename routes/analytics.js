const moment = require('moment-timezone');

const express = require('express');
const config = require('config');

// const mongoose = require('mongoose');
// const { Campaigner } = require('../models/campaigner');
const { Payment } = require('../models/payment');
const router = express.Router();

router.get('/today', async (req, res) => {
	const token = req.header('x-auth-token');
	try {
		const registrationsToday = await Payment.find({
			date: moment()
				.tz('Asia/Kolkata')
				.format('MMM Do YYYY')
		}).countDocuments();

		res.header('Access-Control-Allow-Origin', '*').send({
			auth: {
				registrations_today: registrationsToday
			},
			status: true
		});
	} catch (ex) {
		console.log(ex);
		res.status(500).send({ auth: 'Internal Server Error', status: false });
	}
});

router.get('/tilldate', async (req, res) => {
	const token = req.header('x-auth-token');
	try {
		const registrationsTillDate = await Payment.find({}).countDocuments();

		let paymentsTillDateIeeeYpCash = await Payment.find({
			membershipType: 'IEEE',
			paymentType: 'Cash',
			registrationType: 'Young Professional'
		}).countDocuments();

		paymentsTillDateIeeeYpCash *= config.get('IEEE_fee_yp');

		let paymentsTillDateNonIeeeYpCash = await Payment.find({
			membershipType: 'non-IEEE',
			paymentType: 'Cash',
			registrationType: 'Young Professional'
		}).countDocuments();
		paymentsTillDateNonIeeeYpCash *= config.get('non-IEEE_fee_yp');

		let paymentsTillDateIeeeStuCash = await Payment.find({
			membershipType: 'IEEE',
			paymentType: 'Cash',
			registrationType: 'Student'
		}).countDocuments();
		paymentsTillDateIeeeStuCash *= config.get('IEEE_fee_student');
		let paymentsTillDateNonIeeeStuCash = await Payment.find({
			membershipType: 'non-IEEE',
			paymentType: 'Cash',
			registrationType: 'Student'
		}).countDocuments();
		paymentsTillDateNonIeeeStuCash *= config.get('non-IEEE_fee_student');

		let paymentsTillDateCash =
			paymentsTillDateNonIeeeStuCash +
			paymentsTillDateIeeeStuCash +
			paymentsTillDateNonIeeeYpCash +
			paymentsTillDateIeeeYpCash;

		let paymentsTillDateIeeeYpPayTM = await Payment.find({
			membershipType: 'IEEE',
			paymentType: 'PayTM',
			registrationType: 'Young Professional'
		}).countDocuments();

		paymentsTillDateIeeeYpPayTM *= config.get('IEEE_fee_yp');

		let paymentsTillDateNonIeeeYpPayTM = await Payment.find({
			membershipType: 'non-IEEE',
			paymentType: 'PayTM',
			registrationType: 'Young Professional'
		}).countDocuments();
		paymentsTillDateNonIeeeYpPayTM *= config.get('non-IEEE_fee_yp');

		let paymentsTillDateIeeeStuPayTM = await Payment.find({
			membershipType: 'IEEE',
			paymentType: 'PayTM',
			registrationType: 'Student'
		}).countDocuments();
		paymentsTillDateIeeeStuPayTM *= config.get('IEEE_fee_student');
		let paymentsTillDateNonIeeeStuPayTM = await Payment.find({
			membershipType: 'non-IEEE',
			paymentType: 'PayTM',
			registrationType: 'Student'
		}).countDocuments();
		paymentsTillDateNonIeeeStuPayTM *= config.get('non-IEEE_fee_student');

		let paymentsTillDatePayTM =
			paymentsTillDateNonIeeeStuPayTM +
			paymentsTillDateIeeeStuPayTM +
			paymentsTillDateIeeeYpPayTM +
			paymentsTillDateIeeeYpPayTM;
		res.header('Access-Control-Allow-Origin', '*').send({
			auth: {
				registrations_tilldate: registrationsTillDate,
				payments_tilldatePayTM: paymentsTillDatePayTM,
				payments_tilldateCash: paymentsTillDateCash
			},
			status: true
		});
	} catch (ex) {
		console.log(ex);
		res.status(500).send({ auth: 'Internal Server Error', status: false });
	}
});

router.get('/campaigner', async (req, res) => {
	const token = req.header('x-auth-token');
	try {
		let a = await Payment.aggregate([
			{
				$group: {
					_id: '$campaignerId',
					cash_total: {
						$sum: {
							$cond: [
								{
									$eq: ['$paymentType', 'Cash']
								},
								'$amount',
								0
							]
						}
					},
					paytm_total: {
						$sum: {
							$cond: [
								{
									$eq: ['$paymentType', 'PayTM']
								},
								'$amount',
								0
							]
						}
					},
					total_registered: { $sum: 1 },
					today_registered: {
						$sum: {
							$cond: [
								{
									$eq: [
										'$date',
										moment()
											.tz('Asia/Kolkata')
											.format('MMM Do YYYY')
									]
								},
								1,
								0
							]
						}
					},
					cash_today: {
						$sum: {
							$cond: [
								{
									$and: [
										{ $eq: ['$paymentType', 'Cash'] },
										{
											$eq: [
												'$date',
												moment()
													.tz('Asia/Kolkata')
													.format('MMM Do YYYY')
											]
										}
									]
								},
								'$amount',
								0
							]
						}
					},
					paytm_today: {
						$sum: {
							$cond: [
								{
									$and: [
										{ $eq: ['$paymentType', 'PayTM'] },
										{
											$eq: [
												'$date',
												moment()
													.tz('Asia/Kolkata')
													.format('MMM Do YYYY')
											]
										}
									]
								},
								'$amount',
								0
							]
						}
					}
				}
			},
			{
				$lookup: {
					from: 'campaigners',
					localField: '_id',
					foreignField: '_id',
					as: 'campaigner'
				}
			},
			{
				$unwind: '$campaigner'
			},
			{
				$project: {
					name: '$campaigner.name',
					mobile: '$campaigner.mobile',
					cash_today: '$cash_today',
					paytm_today: '$paytm_today',
					cash_total: '$cash_total',
					paytm_total: '$paytm_total',
					total_registered: '$total_registered',
					today_registered: '$today_registered',
					total_payment_today: '$campaigner.amountCollectedToday'
				}
			}
		]).exec();
		res.send({
			auth: a,
			status: true
		});
	} catch (ex) {
		console.log(ex);
		res.status(500).send({ auth: 'Internal Server Error', status: false });
	}
});
module.exports = router;
