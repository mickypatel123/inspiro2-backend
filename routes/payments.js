const moment = require('moment-timezone');
// const _ = require('lodash');
const express = require('express');
const jwt = require('jsonwebtoken');
const config = require('config');

// const mongoose = require('mongoose');
// const { Campaigner } = require('../models/campaigner');
const { Payment } = require('../models/payment');
const auth = require('../middleware/auth');

const router = express.Router();

router.get('/', auth, async (req, res) => {
	const token = req.header('x-auth-token');
	try {
		const registrationsToday = await Payment.find({
			campaignerId: jwt.verify(token, config.get('jwtPrivateKey'))._id,
			date: moment()
				.tz('Asia/Kolkata')
				.format('MMM Do YYYY')
		}).countDocuments();

		let paymentsTodayIeeeYp = await Payment.find({
			campaignerId: jwt.verify(token, config.get('jwtPrivateKey'))._id,
			date: moment()
				.tz('Asia/Kolkata')
				.format('MMM Do YYYY'),
			membershipType: 'IEEE',
			registrationType: 'Young Professional'
		}).countDocuments();

		paymentsTodayIeeeYp *= config.get('IEEE_fee_yp');

		let paymentsTodayNonIeeeYp = await Payment.find({
			campaignerId: jwt.verify(token, config.get('jwtPrivateKey'))._id,
			date: moment()
				.tz('Asia/Kolkata')
				.format('MMM Do YYYY'),
			membershipType: 'non-IEEE',
			registrationType: 'Young Professional'
		}).countDocuments();
		paymentsTodayNonIeeeYp *= config.get('non-IEEE_fee_yp');

		let paymentsTodayIeeeStu = await Payment.find({
			campaignerId: jwt.verify(token, config.get('jwtPrivateKey'))._id,
			date: moment()
				.tz('Asia/Kolkata')
				.format('MMM Do YYYY'),
			membershipType: 'IEEE',
			registrationType: 'Student'
		}).countDocuments();
		paymentsTodayIeeeStu *= config.get('IEEE_fee_student');
		let paymentsTodayNonIeeeStu = await Payment.find({
			campaignerId: jwt.verify(token, config.get('jwtPrivateKey'))._id,
			date: moment()
				.tz('Asia/Kolkata')
				.format('MMM Do YYYY'),
			membershipType: 'non-IEEE',
			registrationType: 'Student'
		}).countDocuments();
		paymentsTodayNonIeeeStu *= config.get('non-IEEE_fee_student');

		let paymentsToday =
			paymentsTodayNonIeeeStu +
			paymentsTodayIeeeStu +
			paymentsTodayNonIeeeYp +
			paymentsTodayIeeeYp;
		res.send({
			auth: {
				registrations_today: registrationsToday,
				payments_today: paymentsToday
			},
			status: true
		});
	} catch (ex) {
		console.log(ex);
		res.status(500).send({ auth: 'Internal Server Error', status: false });
	}
});
module.exports = router;
