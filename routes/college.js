const express = require('express');
const { College } = require('../models/college');

const router = express.Router();

router.get('/', async (req, res) => {
	const college = await College.find().select('collegeName -_id');

	res.send({
		auth: college,
		status: true
	});
});
module.exports = router;
