const bcrypt = require('bcrypt');
const _ = require('lodash');
const express = require('express');

const mongoose = require('mongoose');
const { Campaigner, validate } = require('../models/campaigner');

const router = express.Router();
router.post('/', async (req, res) => {
	const { error } = validate(req.body);
	if (error) {
		const errorString = error.details[0].message.replace(/\"/g, '');
		return res.send({ auth: errorString, status: true });
	}
	let campaigner = await Campaigner.findOne({ mobile: req.body.mobile });
	if (campaigner) return res.send({ auth: 'Campaigner already registered', status: true });
	campaigner = new Campaigner(
		_.pick(req.body, [
			'name',
			'password',
			'mobile',
			'non_ieee_registrationsToday',
			'ieee_registrationsToday',
			'amountCollectedToday'
		])
	);
	campaigner.password = await bcrypt.hash(campaigner.password, 10);
	await campaigner.save();
	res.send({ auth: 'Successfully Registered', status: true });
});

module.exports = router;
