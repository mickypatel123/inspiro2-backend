const Joi = require('joi');
const bcrypt = require('bcrypt');

const _ = require('lodash');
const express = require('express');

const mongoose = require('mongoose');
const { Campaigner } = require('../models/campaigner');

const router = express.Router();

router.post('/', async (req, res) => {
	const { error } = validate(req.body);
	if (error) {
		const errorString = error.details[0].message.replace(/\"/g, '');
		return res.send({ auth: errorString, status: true });
	}

	let campaigner = await Campaigner.findOne({ mobile: req.body.mobile });
	if (!campaigner) return res.send({ auth: 'Invalid Credetials', status: true });

	const validPassword = await bcrypt.compare(req.body.password, campaigner.password);
	if (!validPassword) return res.send({ auth: 'Invalid Credetials', status: true });

	const token = campaigner.genAuthToken();

	res.header('x-auth-token', token).send({ auth: 'Successfully Logged in', status: true });
});
function validate(req) {
	const schema = {
		mobile: Joi.number()
			.integer()
			.required(),
		password: Joi.string().required()
	};
	return Joi.validate(req, schema);
}
module.exports = router;
