const mongoose = require('mongoose');

const College = mongoose.model(
	'College',
	new mongoose.Schema({
		collegeName: {
			type: String,
			required: true,
			unique: true
		}
	})
);

exports.College = College;
