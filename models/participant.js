const Joi = require('joi');
const mongoose = require('mongoose');

const Participant = mongoose.model(
	'Participant',
	new mongoose.Schema({
		firstname: {
			type: String,
			required: true
		},
		lastname: {
			type: String,
			required: true
		},
		email: {
			type: String,
			required: true,
			unique: true
		},
		mobile: {
			type: Number,
			required: true,
			unique: true
		},
		college: {
			type: String,
			required: true
		},
		registrationType: {
			type: String,
			required: true
		},
		membership: {
			type: Number
		},
		membershipType: {
			type: String
		},
		paymentType: {
			type: String,
			required: true
		},
		registeredBy: {
			type: mongoose.Schema.Types.ObjectId,
			required: true
		}
	})
);

function validateParticipant(participant) {
	const schema = {
		firstname: Joi.string().required(),
		lastname: Joi.string().required(),
		email: Joi.string()
			.required()
			.email(),
		mobile: Joi.number()
			.integer()
			.required(),
		college: Joi.string().required(),
		registrationType: Joi.string().required(),
		paymentType: Joi.string().required(),
		membership: Joi.number()
	};

	return Joi.validate(participant, schema);
}

function findingParticipant(participant) {
	const schema = {
		firstname: Joi.string(),
		lastname: Joi.string(),
		email: Joi.string().email(),
		mobile: Joi.number().integer(),
		college: Joi.string(),
		registrationType: Joi.string(),
		paymentType: Joi.string(),
		membership: Joi.number()
	};

	return Joi.validate(participant, schema);
}
exports.Participant = Participant;
exports.validate = validateParticipant;
exports.findvalidation = findingParticipant;
