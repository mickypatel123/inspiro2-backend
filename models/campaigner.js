const Joi = require('joi');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const config = require('config');
const { cronPlugin } = require('mongoose-cron');
const campaignerSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true
	},
	mobile: {
		type: Number,
		required: true,
		unique: true
	},
	password: {
		type: String,
		required: true
	},
	non_ieee_registrationsToday: {
		type: Number
	},
	ieee_registrationsToday: {
		type: Number
	},
	amountCollectedToday: {
		type: Number
	}
});
campaignerSchema.method('genAuthToken', function() {
	const token = jwt.sign({ _id: this._id }, config.get('jwtPrivateKey'));
	return token;
});

const Campaigner = mongoose.model('Campaigner', campaignerSchema);

function validateCampaigner(campaigner) {
	const schema = {
		name: Joi.string().required(),
		mobile: Joi.number()
			.integer()
			.required(),
		password: Joi.string().required(),
		ieee_registrationsToday: Joi.number().integer(),
		non_ieee_registrationsToday: Joi.number().integer(),
		amountCollectedToday: Joi.number().integer()
	};
	return Joi.validate(campaigner, schema);
}

exports.Campaigner = Campaigner;
exports.validate = validateCampaigner;
