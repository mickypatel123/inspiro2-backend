const mongoose = require('mongoose');

const Payment = mongoose.model(
	'Payment',
	new mongoose.Schema({
		date: {
			type: String,
			required: true
		},
		time: {
			type: String
		},
		participantId: {
			type: mongoose.Schema.Types.ObjectId,
			required: true,
			unique: true
		},
		campaignerId: {
			type: mongoose.Schema.Types.ObjectId,
			required: true
		},
		registrationType: {
			type: String,
			required: true
		},
		membershipType: {
			type: String,
			required: true
		},
		paymentType: {
			type: String,
			required: true
		},
		amount: {
			type: Number,
			required: true
		}
	})
);

exports.Payment = Payment;
