const jwt = require('jsonwebtoken');
const config = require('config');

function auth(req, res, next) {
	let token = req.header('x-auth-token');
	if (!token) return res.send({ auth: 'Access Denied. You are not logged in', status: true });
	try {
		let decoded = jwt.verify(token, config.get('jwtPrivateKey'));
		req.participant = decoded;
		next();
	} catch (ex) {
		res.send({ auth: 'Invalid Token', status: true });
	}
}

module.exports = auth;
